class RenameAccountsToStoreAccounts < ActiveRecord::Migration[5.0]
  def change
    rename_table :accounts, :store_accounts
  end
end
