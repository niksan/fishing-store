class AddDeliveryPage < ActiveRecord::Migration[5.1]
  def change
    Page.create(title: 'Доставка', body: '<p>Доставка осуществляется курьерской службой <a href="https://www.cdek.ru/" target="_blank">SDEK</a>.</p><p>Стоимость доставки расчитывается автоматически, в зависимости от размера, веса посылки и расстояния до пункта назначения</p><p>Доставка осуществлляется по Воронежской области, Липецкой области, Курской области и др.</p>', code: 'delivery')
  end
end
