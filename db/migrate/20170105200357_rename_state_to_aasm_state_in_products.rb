class RenameStateToAasmStateInProducts < ActiveRecord::Migration[5.0]
  def change
    rename_column :products, :state, :aasm_state
  end
end
