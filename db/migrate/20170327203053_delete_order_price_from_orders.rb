class DeleteOrderPriceFromOrders < ActiveRecord::Migration[5.0]
  def change
    remove_column :orders, :order_price
  end
end
