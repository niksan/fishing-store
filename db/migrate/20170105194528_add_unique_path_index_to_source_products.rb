class AddUniquePathIndexToSourceProducts < ActiveRecord::Migration[5.0]

  def up
    change_column :source_products, :path, :string, null: false
    add_index :source_products, :path, unique: true
  end

  def down
    remove_index :source_products, :path
  end

end
