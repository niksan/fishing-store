class AddMinPriceToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :min_price, :decimal, precision: 6, scale: 2
  end
end
