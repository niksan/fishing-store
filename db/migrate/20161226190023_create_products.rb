class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name
      t.decimal :price, precision: 6, scale: 2
      t.text :description
      t.integer :brand_id
      t.integer :category_id
      t.integer :source_product_id
      t.integer :pieces_per_pack, default: 1
      t.integer :min_order, default: 1
      t.boolean :valid, default: false
      t.boolean :published, default: false
    end
  end
end
