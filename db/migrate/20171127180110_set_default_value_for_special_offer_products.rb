class SetDefaultValueForSpecialOfferProducts < ActiveRecord::Migration[5.1]
  def change
    change_column :products, :special_offer, :boolean, default: false
  end
end
