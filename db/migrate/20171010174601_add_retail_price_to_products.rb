class AddRetailPriceToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :retail_price, :decimal, precision: 9, scale: 2
  end
end
