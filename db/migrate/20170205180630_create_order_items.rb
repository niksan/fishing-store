class CreateOrderItems < ActiveRecord::Migration[5.0]
  def change
    create_table :order_items do |t|
      t.integer :order_id
      t.integer :product_id
      t.string :name, default: :null
      t.decimal :unit_price, precision: 9, scale: 2, default: 0
      t.integer :quantity, default: 0
    end
    add_index :order_items, :order_id
  end
end
