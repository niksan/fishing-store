class AddTypeToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :type, :string, default: nil
    Product.update_all(type: 'Product::Piranya')
  end
end
