class AddPriceMarginToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :price_margin, :decimal, precision: 9, scale: 2
  end
end
