class AddUniqueIndexNameDepthForCategories < ActiveRecord::Migration[5.0]
  def change
    add_index :categories, [:name, :depth], unique: true
  end
end
