class AddSpinning36ToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :spinning36, :boolean, default: false
  end
end
