class CreateReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :reviews do |t|
      t.string :name
      t.text :value
      t.string :type
      t.integer :product_id
    end
  end
end
