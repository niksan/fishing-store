class RemovePublishedFromProducts < ActiveRecord::Migration[5.0]
  def change
    remove_column :products, :published
  end
end
