class AddPathToSourceProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :source_products, :path, :string
  end
end
