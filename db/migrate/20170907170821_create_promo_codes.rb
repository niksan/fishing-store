class CreatePromoCodes < ActiveRecord::Migration[5.1]
  def change
    create_table :promo_codes do |t|
      t.string :name
      t.string :code
      t.integer :discount_percentages, default: 0
      t.datetime :created_at
      t.datetime :updated_at
    end
    add_index :promo_codes, :name, unique: true
    add_index :promo_codes, :code, unique: true
  end
end
