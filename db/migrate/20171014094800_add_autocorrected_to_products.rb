class AddAutocorrectedToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :autocorrected, :boolean, default: false
  end
end
