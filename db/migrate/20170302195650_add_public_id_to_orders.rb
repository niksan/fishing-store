class AddPublicIdToOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :public_id, :string
  end
end
