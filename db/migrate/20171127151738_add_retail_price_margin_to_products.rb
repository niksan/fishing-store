class AddRetailPriceMarginToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :retail_price_margin, :decimal, precision: 9, scale: 2, default: 0.0
  end
end
