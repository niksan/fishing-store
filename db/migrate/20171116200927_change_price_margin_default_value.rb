class ChangePriceMarginDefaultValue < ActiveRecord::Migration[5.1]
  def change
    change_column :products, :price_margin, :decimal, precision: 9, scale: 2, default: 0.0
  end
end
