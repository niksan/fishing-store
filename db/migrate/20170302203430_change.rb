class Change < ActiveRecord::Migration[5.0]
  def up
    change_column_default :orders, :user_id, nil
    change_column_default :orders, :first_name, nil
    change_column_default :orders, :last_name, nil
    change_column_default :orders, :phone_number, nil
    change_column_default :orders, :email, nil
    change_column_default :orders, :aasm_state, nil
    change_column_default :orders, :shipping_address, nil
    change_column_default :orders, :shipping_method, nil
  end
  
  def down
  end
end
