class AddDepthToProductImages < ActiveRecord::Migration[5.0]
  def change
    add_column :product_images, :depth, :integer
  end
end
