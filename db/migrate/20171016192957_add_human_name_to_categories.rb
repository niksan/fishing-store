class AddHumanNameToCategories < ActiveRecord::Migration[5.1]
  def change
    add_column :categories, :human_name, :string
  end
end
