class RenameStoreNameToTypeInStoreAccounts < ActiveRecord::Migration[5.0]
  def change
    rename_column :store_accounts, :store_name, :type
  end
end
