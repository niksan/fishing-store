class AddShippingInformationToOrder < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :destination_id, :integer
    add_column :orders, :city, :string
    add_column :orders, :region, :string
  end
end
