class CreateFieldValues < ActiveRecord::Migration[5.0]
  def change
    create_table :field_values do |t|
      t.integer :product_id
      t.integer :field_id
      t.string :value
    end
    add_index :field_values, [:product_id, :field_id], unique: true
  end
end
