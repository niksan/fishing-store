class CreateFieldsets < ActiveRecord::Migration[5.0]
  def change
    create_table :fieldsets do |t|
      t.string :name
    end
    create_table :fieldset_fields do |t|
      t.integer :fieldset_id
      t.integer :field_id
    end
    add_index :fieldset_fields, [:field_id, :fieldset_id], unique: true
  end
end
