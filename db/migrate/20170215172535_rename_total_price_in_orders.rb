class RenameTotalPriceInOrders < ActiveRecord::Migration[5.0]
  def change
    rename_column :orders, :total_price, :order_price
  end
end
