class RenamePathToUrlInSourceProducts < ActiveRecord::Migration[5.0]
  def change
    remove_index :source_products, :path
    rename_column :source_products, :path, :url
    add_index :source_products, :url, unique: true
  end
end
