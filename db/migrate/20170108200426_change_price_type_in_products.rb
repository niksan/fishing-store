class ChangePriceTypeInProducts < ActiveRecord::Migration[5.0]
  def up
    change_column :products, :price, :decimal, precision: 9, scale: 2
    change_column :products, :min_price, :decimal, precision: 9, scale: 2
    change_column :source_products, :price, :decimal, precision: 9, scale: 2
  end

  def down
  end
end
