class AddParentIdToProductImages < ActiveRecord::Migration[5.0]
  def change
    add_column :product_images, :parent_id, :integer
  end
end
