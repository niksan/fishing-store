class AddProductPrefixToCategories < ActiveRecord::Migration[5.1]
  def change
    add_column :categories, :product_prefix, :string
  end
end
