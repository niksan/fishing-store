class ChangeStoreToTypeInSourceProducts < ActiveRecord::Migration[5.0]
  def change
    rename_column :source_products, :store, :type
  end
end
