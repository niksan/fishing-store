class AddBrowserSessionHashToOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :browser_session_hash, :string
  end
end
