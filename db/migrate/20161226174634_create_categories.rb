class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
      t.string :name
      t.text :description
      t.string :image
      t.integer :parent_id, default: 0
      t.integer :depth, default: 0
      t.integer :position, default: 0
    end
  end
end
