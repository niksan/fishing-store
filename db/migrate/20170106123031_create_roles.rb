class CreateRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :roles do |t|
      t.string :name
    end
    add_index :roles, :name, unique: :true
    create_table :roles_users do |t|
      t.integer :role_id
      t.integer :user_id
    end
    add_index :roles_users, [:role_id, :user_id], unique: :true
    Role.create(name: 'super_admin')
  end
end
