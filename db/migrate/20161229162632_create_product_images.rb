class CreateProductImages < ActiveRecord::Migration[5.0]
  def change
    create_table :product_images do |t|
      t.string :file
      t.boolean :main
      t.integer :position
      t.integer :product_id
    end
  end
end
