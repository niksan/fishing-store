class CreatePayments < ActiveRecord::Migration[5.0]
  def change
    create_table :payments do |t|
      t.integer :order_id
      t.decimal :amount
      t.text :external_data
      t.string :aasm_state
      t.string :type
    end
  end
end
