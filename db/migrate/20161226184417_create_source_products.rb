class CreateSourceProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :source_products do |t|
      t.string :name
      t.decimal :price, precision: 6, scale: 2
      t.boolean :valid, default: false
      t.text :parsed_json
      t.text :source
      t.string :store
    end
  end
end
