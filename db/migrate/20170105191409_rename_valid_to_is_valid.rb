class RenameValidToIsValid < ActiveRecord::Migration[5.0]
  def change
    rename_column :source_products, :valid, :is_valid
    rename_column :products, :valid, :is_valid
  end
end
