class CreateNewRoleMarketVendor < ActiveRecord::Migration[5.1]
  def self.up
    Role.create(name: 'market_vendor')
  end

  def self.down
    Role.find_by(name: 'market_vendor').destroy
  end
end
