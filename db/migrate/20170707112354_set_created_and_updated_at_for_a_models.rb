class SetCreatedAndUpdatedAtForAModels < ActiveRecord::Migration[5.1]
  def change
    add_column :brands, :created_at, :datetime
    add_column :brands, :updated_at, :datetime
    Brand.where(created_at: nil).update_all(created_at: Time.zone.now)
    Brand.where(updated_at: nil).update_all(updated_at: Time.zone.now)

    add_column :categories, :created_at, :datetime
    add_column :categories, :updated_at, :datetime
    Category.where(created_at: nil).update_all(created_at: Time.zone.now)
    Category.where(updated_at: nil).update_all(updated_at: Time.zone.now)

    add_column :fields, :created_at, :datetime
    add_column :fields, :updated_at, :datetime
    Field.where(created_at: nil).update_all(created_at: Time.zone.now)
    Field.where(updated_at: nil).update_all(updated_at: Time.zone.now)

    add_column :field_values, :created_at, :datetime
    add_column :field_values, :updated_at, :datetime
    FieldValue.where(created_at: nil).update_all(created_at: Time.zone.now)
    FieldValue.where(updated_at: nil).update_all(updated_at: Time.zone.now)

    add_column :fieldsets, :created_at, :datetime
    add_column :fieldsets, :updated_at, :datetime
    Fieldset.where(created_at: nil).update_all(created_at: Time.zone.now)
    Fieldset.where(updated_at: nil).update_all(updated_at: Time.zone.now)

    add_column :pages, :created_at, :datetime
    add_column :pages, :updated_at, :datetime
    Page.where(created_at: nil).update_all(created_at: Time.zone.now)
    Page.where(updated_at: nil).update_all(updated_at: Time.zone.now)

    add_column :payments, :created_at, :datetime
    add_column :payments, :updated_at, :datetime
    Payment.where(created_at: nil).update_all(created_at: Time.zone.now)
    Payment.where(updated_at: nil).update_all(updated_at: Time.zone.now)

    add_column :payment_versions, :updated_at, :datetime
    PaymentVersion.where(updated_at: nil).update_all(updated_at: Time.zone.now)

    Product.where(created_at: nil).update_all(created_at: Time.zone.now)
    Product.where(updated_at: nil).update_all(updated_at: Time.zone.now)

    add_column :product_versions, :updated_at, :datetime
    ProductVersion.where(updated_at: nil).update_all(updated_at: Time.zone.now)

    add_column :product_images, :created_at, :datetime
    add_column :product_images, :updated_at, :datetime
    ProductImage.where(created_at: nil).update_all(created_at: Time.zone.now)
    ProductImage.where(updated_at: nil).update_all(updated_at: Time.zone.now)

    add_column :reviews, :created_at, :datetime
    add_column :reviews, :updated_at, :datetime
    Review.where(created_at: nil).update_all(created_at: Time.zone.now)
    Review.where(updated_at: nil).update_all(updated_at: Time.zone.now)

    add_column :roles, :created_at, :datetime
    add_column :roles, :updated_at, :datetime
    Role.where(created_at: nil).update_all(created_at: Time.zone.now)
    Role.where(updated_at: nil).update_all(updated_at: Time.zone.now)

    add_column :source_products, :created_at, :datetime
    add_column :source_products, :updated_at, :datetime
    SourceProduct.where(created_at: nil).update_all(created_at: Time.zone.now)
    SourceProduct.where(updated_at: nil).update_all(updated_at: Time.zone.now)

    add_column :source_product_versions, :updated_at, :datetime
    SourceProductVersion.where(updated_at: nil).update_all(updated_at: Time.zone.now)

    add_column :store_accounts, :created_at, :datetime
    add_column :store_accounts, :updated_at, :datetime
    StoreAccount.where(created_at: nil).update_all(created_at: Time.zone.now)
    StoreAccount.where(updated_at: nil).update_all(updated_at: Time.zone.now)
  end
end
