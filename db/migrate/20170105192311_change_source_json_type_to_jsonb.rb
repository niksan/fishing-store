class ChangeSourceJsonTypeToJsonb < ActiveRecord::Migration[5.0]
  def change
    remove_column :source_products, :parsed_json
    add_column :source_products, :parsed_json, :jsonb
  end
end
