class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.integer :user_id, default: :null
      t.string :first_name, default: :null
      t.string :last_name, default: :null
      t.string :phone_number, default: :null
      t.string :email, default: :null
      t.string :aasm_state, default: :null
      t.text :shipping_address, default: :null
      t.decimal :shipping_price, precision: 9, scale: 2, default: 0
      t.string :shipping_method, default: :null
      t.decimal :total_price, precision: 9, scale: 2, default: 0
    end
    add_index :orders, :user_id
  end
end
