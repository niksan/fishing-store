class Order < ApplicationRecord
  include AASM

  has_many :order_items, dependent: :destroy
  has_one :payment
  belongs_to :user
  validates :browser_session_hash, presence: true
  validate :destination_id_rules
  validate :check_order_items
  before_save :set_shipping_price

  aasm do
    state :initialized, initial: true
    state :ready_to_pay, :paid, :delivering, :completed, :rejected

    event :to_ready_to_pay do
      transitions from: :initialized, to: :ready_to_pay, if: :can_make_order?
    end

    event :to_pay do
      transitions from: :ready_to_pay, to: :paid
      after do
        OrderMailer.complete_order_customer(self.id).deliver_later!
        OrderMailer.complete_order_vendor(self.id).deliver_later!
        decrement_products!
      end
    end

    event :to_delivering do
      transitions from: :paid, to: :delivering
    end

    event :to_complete do
      transitions from: [:paid, :delivering], to: :complete
    end

    event :reject do
      transitions from: [:paid, :ready_to_pay, :delivering], to: :rejected
    end

  end

  def products_price
    order_items.sum { |oi| oi.full_price }.to_f
  end

  def total_price
    products_price + shipping_price.to_f
  end

  def add_product(product_id, quantity=1)
    save! unless persisted? #save order sesion on click buy button
    order_item = OrderItem.create_with(quantity: 0).find_or_create_by!(order_id: id, product_id: product_id)
    order_item.increment_quantity(quantity)
    order_item.save!
    save!
  end

  def can_make_order?
    items_condition = (order_items.map { |oi| oi.is_it_full_valid? }.exclude?(false))
    items_condition && destination_id.present? && shipping_price.nonzero? && shipping_address.present?
  end

  def destination_name
    SdekDestination.find_by_id(destination_id).try(:full_name)
  end

  def customer_full_name
    "#{first_name} #{last_name}"
  end

  def decrement_products!
    order_items.each do |order_item|
      new_product_quantity = order_item.product.quantity - order_item.quantity
      order_item.product.update_attributes(quantity: new_product_quantity)
    end
  end

  protected

  def set_shipping_price
    if destination_id.present?
      self.shipping_price = Client::Sdek.new.calculate(destination_id, 506)
    end
  end

  def destination_id_rules
    if destination_id.present? && SdekDestination.all.map(&:sdek_id).exclude?(destination_id)
      errors.add(:quantity, "Wrong destination_id.")
    end
  end

  def check_order_items
    invalid_items = order_items.detect { |order_item| !order_item.valid? }
    self.errors.add(:product_quantity, 'Invalid product in order!') if invalid_items
  end

end
