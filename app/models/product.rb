class Product < ApplicationRecord
  include AASM
  
  has_paper_trail class_name: 'ProductVersion'

  validates :name, presence: true
  validates :price, presence: true
  validates :category_id, presence: true

  has_many :product_images, dependent: :destroy
  has_many :field_values, dependent: :destroy
  has_many :fields, through: :field_values
  has_many :reviews
  belongs_to :category
  belongs_to :brand
  belongs_to :source_product, dependent: :destroy

  scope :initialized, -> { where(aasm_state: 'initialized') }
  scope :moderation, -> { where(aasm_state: 'moderation') }
  scope :published, -> { joins(:category).where(aasm_state: 'published', is_valid: true, categories: { published: true }).where('products.quantity > ?', 0) }
  scope :rejected, -> { where(aasm_state: 'rejected') }
  scope :ready_to_show, -> { published.includes(:product_images, :brand).references(:product_images, :brand) }
  scope :without_source_product, -> { includes(:source_product).where(source_products: { id: nil }) }
  scope :with_source_product, -> { joins(:source_product) }
  scope :special_offer, -> { where(special_offer: true) }

  scope :moderated_price, -> { joins(:source_product).where(aasm_state: 'moderation').where("products.price > source_products.price") }

  after_commit :delete_view_cache, on: [:update, :destroy]
  before_save :check_availability!
  before_save :set_min_price

  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  accepts_nested_attributes_for :product_images, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :field_values, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :reviews, reject_if: :all_blank, allow_destroy: true

  MIN_COMMISSION_COEFF = 0.35

  class << self
    include Rails.application.routes.url_helpers
    
    def yandex_market_arr
      Rails.cache.fetch('yandex_market_products_arr', expires_in: 1.day) do
        res = []
        Product.ready_to_show.where.not(brand_id: nil).find_each do |product|
          res << {
            id: product.id, 
            brand_name: product.brand.name,
            name: product.name,
            url: product_url(product[:id], host: 'fisher-secrets.ru', protocol: 'https', port: nil),
            price: product.price.to_i,
            category_id: product.category_id,
            picture_url: "https://#{@host}#{product.main_image.file.medium.url}",
            description: product.description,
            min_order: product.min_order
          }
        end
        res
      end
    end

  end

  aasm do
    state :initialized, initial: true
    state :moderation, :published, :rejected

    event :to_moderate do
      transitions to: :moderation
    end

    event :publish do
      transitions from: :moderation, to: :published, if: :can_be_published?
    end

    event :reject do
      transitions to: :rejected
    end

  end

  def source_price
    source_product.try(:price).to_f
  end

  def discount_price(user)
    extra_percents = 15
    if user.try(:spinning36) && source_product.present?
      extra = source_price/100.0 * extra_percents
      source_price + extra
    else
      nil
    end
  end

  def real_price(user)
    discount_price(user) || price
  end

  def can_be_published?
    has_images = product_images.where(product_images: { main: true }).any?
    source_price_cond = if source_product.present?
      price > source_price
    else
      true
    end
    quantity > 0 && price.nonzero? && source_price_cond && has_images
  end
  
  def cache_name_for_short_view
    "short_product_info_#{ id }"
  end
  
  def cache_name_for_full_view
    "full_product_info_#{ id }"
  end

  def delete_view_cache
    Rails.cache.delete(cache_name_for_short_view)
    Rails.cache.delete(cache_name_for_full_view)
  end

  def main_image
    product_images.first
  end

  def full_name
    "#{brand.try(:name)} #{name}"
  end

  def can_be_sale?
    Product.published.pluck(:id).include?(id)
  end

  def check_availability!
    if quantity < 1 && aasm_state != 'rejected'
      reject
    end
  end

  def set_min_price!
    set_min_price
    save!
  end

  protected

  def set_min_price
    self.min_price = price * min_order
  end

end
