class SdekDestination

  attr_reader :sdek_id, :city, :region, :full_name
  
  class << self

    def all
      Client::Sdek.new.cities_data.map do |city_data|
        new(city_data[:id], city_data[:city], city_data[:region])
      end
    end

    def find_by_full_name_first_symbols(str)
      all.select do |obj|
        obj.full_name.match(/^#{str}/i)
      end
    end

    def find_by_id(srch_id)
      all.detect { |obj| obj.sdek_id == srch_id }
    end

  end

  def initialize(id, city, region)
    @sdek_id = id
    @city = city
    @region = region
    @full_name = "#{city}, #{region}"
  end

end
