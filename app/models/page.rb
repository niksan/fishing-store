class Page < ApplicationRecord

  validates :title, presence: true
  validates :code, presence: true

  extend FriendlyId
  friendly_id :title, use: [:history, :finders]

end
