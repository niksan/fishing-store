class Brand < ApplicationRecord

  validates :name, presence: true
  mount_uploader :image, BrandImageUploader

  extend FriendlyId
  friendly_id :name, use: [:history, :finders]

  def self.from_category(category_id)
    brand_ids = Product.ready_to_show
                       .where(category_id: category_id)
                       .uniq.pluck(:brand_id)
    where(id: brand_ids)
  end

end
