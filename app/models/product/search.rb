class Product::Search

  DEFAULT_SOURCE_COLLECTION = ->{ Product.all }

  def self.build(params, source_collection=DEFAULT_SOURCE_COLLECTION)
    new(params, source_collection).build
  end
  
  def initialize(params={}, source_collection=DEFAULT_SOURCE_COLLECTION)
    @params = {}
    # standartize params. Keys is always Symbols, values is a String.
    params.each do |k, v|
      @params[k.to_sym] = v.to_s
    end
    @collection = source_collection
  end

  def build
    prepare_collection
    filter_by_category
    filter_by_brand
    filter_by_price
    @collection
  end

  protected

  def filter_by_category
    if @params[:category_id].present?
      @collection = @collection.where(category_id: @params[:category_id].to_i)
    end
    if @params[:category].present?
      category_id = Category.find_by_friendly_id_slug(@params[:category]).try(:id)
      @collection = @collection.where(category_id: category_id) if category_id
    end
  end

  def filter_by_brand
    if @params[:brand_id].present?
      @collection = @collection.where(brand_id: @params[:brand_id].to_i)
    end
    if @params[:brand].present?
      brand_id = Brand.find_by_friendly_id_slug(@params[:brand]).try(:id)
      @collection = @collection.where(brand_id: brand_id) if brand_id
    end
  end

  def filter_by_price
    if @params[:min_price].present?
      @collection = @collection.where('price >= ?', @params[:min_price].to_f)
    end
    if @params[:max_price].present?
      @collection = @collection.where('price <= ?', @params[:max_price].to_f)
    end
  end

  def prepare_collection
    @collection = @collection.call if @collection.is_a?(Proc)
  end

end
