class Product::Piranya < Product

  validate :valid_price_value?

  before_save :set_price_margin
  before_save :set_retail_price_margin

  scope :for_autopublish, -> { moderation.joins(:source_product).where('min_order = 1') }

  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  class << self

    def autocorrect!(force=false)
      joins(:brand).where(autocorrected: (false|force)).find_each(&:autocorrect!)
    end

    def autopublish!
      for_autopublish.find_each do |p|
        p.autopublish!
      end
    end

  end

  def autocorrect!
    name_rule = brand.name.gsub('ё', 'е')
    new_name = name
                .gsub('ё', 'е')
                .gsub(/#{name_rule}/i, '')
                .gsub('  ', ' ')
    description_rule = '<img width="\d+" src="(\S)*" height="\d+" border="\d+" hspace="\d+">'
    new_description = description.gsub(/#{description_rule}/i, '')
    self.update_attributes(name: new_name, description: new_description, autocorrected: true)
  end

  def autopublish!
    set_autopublish_price
    save! #TODO try to do it just with publish! method
    if can_be_published?
      publish!
    else
      update_from_source
      can_be_published? ? publish! : reject!
    end
  end

  def set_autopublish_price
    # logick uses piranya prices
    #  if retail_price.to_f.nonzero? && retail_price > source_product.price
    #    self.price = retail_price
    #  end
    #with retail_price
    price_with_coeff = (source_product.price + (source_product.price * MIN_COMMISSION_COEFF))
    self.price = if retail_price.to_f.nonzero? && (retail_price.to_f < price_with_coeff)
      retail_price
    else
      price_with_coeff
    end
  end

  def set_price_margin
    if source_price.to_f.nonzero? && price.to_f.nonzero?
      self.price_margin = price/(source_price.to_f/100)-100
    end
  end

  def set_retail_price_margin
    if source_price.to_f.nonzero? && retail_price.to_f.nonzero?
      self.retail_price_margin = retail_price/(source_price.to_f/100)-100
    end
  end

  def update_from_source_product
    if published?
      Rails.logger.info('Product must be unpublished for update from source_product')
      false
    else
      update_attributes(source_product.attributes_for_product)
    end
  end

  def update_from_source
    source_product.update_from_source
    update_from_source_product
  end

  protected

  def valid_price_value?
    source_price < price.to_f
  end

end
