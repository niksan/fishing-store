class Product::Market < Product

  belongs_to :user
  validates :user_id, presence: true

  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]
end
