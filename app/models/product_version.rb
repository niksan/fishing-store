class ProductVersion < PaperTrail::Version
  self.table_name = :product_versions
  self.sequence_name = :product_versions_id_seq
end
