class Review < ApplicationRecord

  validates :name, presence: true
  validates :value, presence: true
  validates :product_id, presence: true

  belongs_to :product

  after_commit :touch_product

  def to_s
    "<a href='#{value}' target='_blank'>#{name}<a/>"
  end

  protected

  #for update view cache
  def touch_product
    product.touch
  end

end
