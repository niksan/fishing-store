class Payment < ApplicationRecord
  include AASM
  has_paper_trail class_name: 'PaymentVersion'

  belongs_to :order

  validates :order_id, presence: true
  validates :amount, presence: true
  validates :aasm_state, presence: true
  validates :type, presence: true

  aasm do
    state :initialized, initial: true
    state :confirmed, :rejected

    event :confirm do
      transitions from: :initialized, to: :confirmed
      after do
        PaymentMailer.complete_payment(self.id).deliver_later!
      end
    end

    event :reject do
      transitions from: :initialized, to: :rejected
    end

  end

end
