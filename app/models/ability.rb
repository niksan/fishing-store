class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    can :read, :all
    can :manage, Order, { user_id: [nil, user.try(:id)], aasm_state: [nil, 'initialized'] }
    can :manage, OrderItem#, { order: { user_id: [nil, user.try(:id)] } }
    can :manage, Product::Market, { user_id: user.try(:id) }
    if user.has_role? :super_admin
      can :manage, :all
    end
  end
end
