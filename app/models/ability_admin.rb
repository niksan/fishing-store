class AbilityAdmin
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    can :manage, OrderItem#, { order: { user_id: [nil, user.try(:id)] } }
    if user.has_role? :super_admin
      can :manage, :all
    else
      cannot :read, :all
    end
  end
end
