class SourceProduct < ApplicationRecord

  #has_paper_trail class_name: 'SourceProductVersion'

  validates :name, presence: true
  validates :price, presence: true
  validates :source, presence: true
  validates :parsed_json, presence: true
  validates :url, presence: true, uniqueness: true
  
  serialize :parsed_json, Hash

  has_one :product
  belongs_to :accountable, polymorphic: true

  scope :valid, -> { where(is_valid: true) }
  scope :invalid, -> { where(is_valid: false) }

end
