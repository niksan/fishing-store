class PaymentVersion < PaperTrail::Version
  self.table_name = :payment_versions
  self.sequence_name = :payment_versions_id_seq
end
