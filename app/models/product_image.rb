class ProductImage < ApplicationRecord

  validates :file, presence: true
  validates :product_id, presence: true
  mount_uploader :file, ProductImageUploader

  include Movable
  movable scope: [:product_id]

  belongs_to :product

  def reprocess_versions!
    self.file.cache_stored_file! 
    self.file.retrieve_from_cache!(self.file.cache_name) 
    self.file.recreate_versions! 
    self.save! 
  end

end
