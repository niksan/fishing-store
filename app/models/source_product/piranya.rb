class SourceProduct::Piranya < SourceProduct

  class << self

    def create_from_source(data, account)
      new_attributes = attributes_from_data(data).merge(
        { accountable_type: 'StoreAccount::Piranya', accountable_id: account.id }
      )
      SourceProduct::Piranya.create!(new_attributes)
    end

    def attributes_from_data(data)
      {
        name: data[:name],
        price: data[:price],
        is_valid: data[:is_valid],
        parsed_json: data,
        source: data[:source],
        url: data[:url]
      }
    end

  end

  def create_product!
    if is_valid && parsed_json[:images].any? && parsed_json[:description].present?
      ActiveRecord::Base.transaction do
        product = Product::Piranya.create!(attributes_for_product)
        parsed_json[:options].each do |option|
          field = Field.where(name: option[:name].strip).first_or_create!
          FieldValue.create!(product_id: product.id, field_id: field.id, value: option[:value])
        end
        parsed_json[:images].each_with_index do |source_image, index|
          product.product_images.create!(remote_file_url: source_image, main: index.zero?) if is_image_url?(source_image)
        end
        product
      end
    end
  end

  def is_it_present_in_external_store?
    accountable.client.product_is_present?(self)
  end

  def attributes_for_product
    brand_name = parsed_json[:options].detect { |k| k[:name] == 'Брэнд' }.try(:[], :value)
    brand = Brand.where(name: brand_name).first_or_create
    category = Category.find_or_create_by!(name: parsed_json[:category_name]) do |cat|
      cat.published = false
    end
    {
      name: name,
      price: price,
      retail_price: parsed_json[:retail_price],
      description: parsed_json[:description],
      brand_id: brand.try(:id),
      category_id: category.try(:id),
      source_product_id: id,
      pieces_per_pack: parsed_json[:pieces_per_pack],
      min_order: parsed_json[:min_order],
      is_valid: is_valid
    }
  end

  def update_from_source
    data = accountable.client.get_product(url)
    return false unless data
    new_attributes = self.class.attributes_from_data(data)
    update_attributes(new_attributes)
  end

  private

  def is_image_url?(url)
    url.match(/.*\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)+/)
  end

end
