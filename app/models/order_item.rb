class OrderItem < ApplicationRecord

  validates :order_id, presence: true
  validates :product_id, presence: true
  validates :quantity, presence: true, numericality: true
  validate :check_product_available
  validate :check_product_type

  belongs_to :order
  belongs_to :product

  before_save :set_product_info!
  after_commit :set_order_order_price, on: [:create, :update]
  after_destroy :set_order_order_price

  AVAILABLE_PRODUCT_TYPES = ['Product::Piranya', 'Product', '']

  def increment_quantity(quantity_to_add=1)
    self.quantity += quantity_to_add
  end

  def full_price
    unit_price * quantity
  end

  def is_it_full_valid?
    valid? && valid_by_product? && is_it_present_in_external_store?
  end

  protected
  
  def set_order_order_price
    order.touch
  end

  def check_product_available
    unless valid_by_product?
      errors.add(:quantity, "Product not avaible to a sale.")
    end
  end

  def set_product_info!
    self.name = product.full_name
    self.unit_price = product.real_price(order.try(:user))
  end

  def valid_by_product?
    product.can_be_sale? && !(quantity > product.quantity) && product.quantity >= 1
  end

  def is_it_present_in_external_store?
    if product.source_product_id
      product.source_product.is_it_present_in_external_store?
    else
      true
    end
  end

  def check_product_type
    product.type.in?(AVAILABLE_PRODUCT_TYPES)
  end

end
