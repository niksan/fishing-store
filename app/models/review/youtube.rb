class Review::Youtube < Review

  def to_s
    "<iframe width='560' height='315' src='https://www.youtube.com/embed/#{value}' frameborder='0' allowfullscreen></iframe>"
  end

end
