class Field < ApplicationRecord

  validates :name, presence: true
  
  has_many :field_values
  has_and_belongs_to_many :fieldsets

end
