class Category::Market < Category

  class << self

    def for_menu
      includes(:products)
        .where(parent_id: 0, published: true)
        .where(products: { aasm_state: 'published', is_valid: true, type: ['Product::Market'] })
        .order(:name)
    end

    def cache_name_categories_tree_hash
      'store_categories_tree_hash_market'
    end

    def cache_name_categories_tree_view
      'web_shop_categories_tree_view_market'
    end

    def tree_hash
      Rails.cache.fetch(cache_name_categories_tree_hash, expires_in: 1.day) do
        tree_hash = {}
        Category::Market.for_menu.each do |category|
          tree_hash[category.id] = {}
          tree_hash[category.id][:name] = category.name
          tree_hash[category.id][:human_name] = category.human_name
          tree_hash[category.id][:slug] = category.slug
          tree_hash[category.id][:parent_id] = category.parent_id
          tree_hash[category.id][:link] = category_path(category)
        end
        tree_hash
      end
    end

    def clear_cache
      Rails.cache.delete(cache_name_categories_tree_hash)
      Rails.cache.delete(cache_name_categories_tree_view)
    end

  end

end
