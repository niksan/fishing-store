class PromoCodes < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true
  validates :discount_percentages, numericality: { less_than_or_equal_to: 100,  only_integer: true }
  before_save :transform_code

  class << self

    def price_with_promo(code, products_price, shipping_price)
      formatted_code = prepare_code(code)
      promo = find_by(name: formatted_code)
      promo.get_price(products_price, shipping_price)
    end

  end

  def get_price(products_price, shipping_price)
    price = products_price + shipping_price
  end

  protected

  def transform_code
    self.code = prepare_code(code)
  end

  private

  def prepare_code(code)
    code.downcase.delete('\'"')
  end

end
