module Movable
  class InvalidPositionError < StandardError; end
  extend ActiveSupport::Concern
  
  included do
    validates :position, presence: true,  numericality: { only_integer: true }
    before_validation :set_position
    scope :ordered, -> { order(position: :desc) }
    after_destroy :after_destroy_changes
  end

  def change_position(prev_id: nil, next_id: nil)
    return unless prev_id || next_id
    if next_id
      move_to_left_of(next_id)
    elsif prev_id
      move_to_right_of(prev_id)
    else
      raise InvalidPositionError, "Undefined position parameter!"
    end
  end

  def next
    scoped_class.where('position > ?', self.position).first
  end

  def prev
    scoped_class.where('position < ?', self.position).first
  end

  def max_pos
    scoped_class.maximum(:position)
  end

  def min_pos
    scoped_class.maximum(:position)
  end

  def move_to_left_of(item_id)
    move_to_side_of(item_id, :left)
  end

  def move_to_right_of(item_id)
    move_to_side_of(item_id, :right)
  end

  def to_position(position)
    if position > 0
      position = self.max_pos if position > self.max_pos
      now_in_pos = scoped_class.find_by(position: position)
      if self.position < position
        move_to_left_of(now_in_pos.id)
      else
        move_to_right_of(now_in_pos.id)
      end
    else
      raise InvalidPositionError, "position can't be less then 0(zero)"
    end
  end

  def after_destroy_changes
      next_item = self.next
      scoped_class.where('position > ?', self.position).update_all('position = position - 1') if next_item
  end

  class_methods do
    def movable(params={ scope: [] })
      @@scoped_fields = params[:scope]
    end
  end

  private

    def move_to_side_of(item_id, side)
      item_pos = parent_class(self.class.to_s).select(:position).find(item_id).position
      pos_arr = [item_pos, self.position].sort
      excl_ids = [self.id]
      upd_operator, new_pos, excl_ids = if self.position < item_pos
                                          if side == :left
                                            ['-', item_pos, excl_ids]
                                          else
                                            ['-', (item_pos-1), (excl_ids << item_id)]
                                          end
                                        else
                                          if side == :left
                                            ['+', (item_pos+1), (excl_ids << item_id)]
                                          else
                                            ['+', item_pos, excl_ids] if side == :right
                                          end
                                        end
      ActiveRecord::Base.transaction do
        scoped_class.where('id NOT IN (?)', excl_ids)
                    .where('position BETWEEN ? AND ?', pos_arr[0], pos_arr[1])
                    .update_all("position = position #{upd_operator} 1")
        self.update_attribute(:position, new_pos)
      end
    end

    def set_position
      if self.new_record?
        last_position = ((scoped_class.select(:position).order(:position).last.try(:position)) || 0) + 1
        self.position = last_position
      end
    end

    def scoped_class
      scoped_fields = @@scoped_fields
      class_to_call = parent_class(self.class.to_s).unscoped
      if scoped_fields
        condition_hash = {}
        scoped_fields = [scoped_fields] unless scoped_fields.is_a?(Array)
        scoped_fields.each do |field|
          condition_hash[field] = self.send(field)
        end
        class_to_call.where(condition_hash)
      else
        class_to_call
      end
    end

    def parent_class(str_class_name)
      str_class_name.split('::').first.constantize #call to parent class if STI
    end
  
end
