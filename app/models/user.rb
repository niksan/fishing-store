class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_and_belongs_to_many :roles

  def has_role?(role_name)
    return true if self.roles.map(&:name).include?('super_admin')
    !!self.roles.find_by(name: role_name.to_s.downcase)
  end

end
