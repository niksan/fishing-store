class SourceProductVersion < PaperTrail::Version
  self.table_name = :source_product_versions
  self.sequence_name = :source_product_versions_id_seq
end
