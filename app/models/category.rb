class Category < ApplicationRecord

  has_many :products

  validates :name, presence: true, uniqueness: { scope: :depth }
  mount_uploader :image, CategoryImageUploader
  
  after_commit :delete_view_cache, on: [:update, :destroy]

  #include Movable
  #movable scope: [:parent_id, :depth]

  extend FriendlyId
  friendly_id :name, use: [:history, :finders]

  scope :root, -> { where(parent_id: 0) }

  class << self
    include Rails.application.routes.url_helpers

    def for_menu
      includes(:products)
        .where(parent_id: 0, published: true)
        .where(products: { aasm_state: 'published', is_valid: true, type: ['Product::Piranya', nil] })
        .order(:name)
    end

    def cache_name_categories_tree_view
      'web_shop_categories_tree_view'
    end

    def cache_name_categories_tree_hash
      'web_shop_categories_tree_hash'
    end

    def tree_hash
      Rails.cache.fetch(cache_name_categories_tree_hash, expires_in: 1.day) do
        tree_hash = {}
        Category.for_menu.each do |category|
          tree_hash[category.id] = {}
          tree_hash[category.id][:name] = category.name
          tree_hash[category.id][:human_name] = category.human_name
          tree_hash[category.id][:slug] = category.slug
          tree_hash[category.id][:parent_id] = category.parent_id
          tree_hash[category.id][:link] = category_path(category)
          category_brands = []
          category.brands.each do |brand|
            category_brands << { id: brand.id, name: brand.name, slug: brand.slug, link: category_path(category, brand: brand.slug) }
          end
          tree_hash[category.id][:brands] = category_brands
        end
        tree_hash
      end
    end

    def array_for_select
      Rails.cache.fetch('array_for_select', expires_in: 7.days) do
        all.order(:name).pluck(:name, :id)
      end
    end

  end

  def parent
    self.class.find(parent_id)
  end

  def children
    self.class.where(parent_id: id)
  end

  def delete_view_cache
    Rails.cache.delete(self.class.cache_name_categories_tree_view)
  end

  def field_ids
    products.joins(:field_values).select('DISTINCT(field_values.field_id)').map { |p| p['field_id'] }.sort
  end

  def fields
    Field.find(field_ids)
  end

  def field_ids_available_for_a_search
    arrays = products.joins(:field_values)
                     .select('products.id AS product_id', 'array_agg(DISTINCT(field_values.field_id)) AS field_ids')
                     .group('products.id')
                     .map{ |p| p['field_ids'] }.uniq
    arrays.inject(){ |memo, arr| memo & arr }.try(:compact)
  end

  def fields_available_for_a_search
    ids = field_ids_available_for_a_search
    Field.find(ids) if ids
  end

  def brand_ids
    products.uniq.pluck(:brand_id)
  end

  def brands
    Brand.where(id: brand_ids).order(:name)
  end

  def piranya_products
    products.where(type: 'Product::Piranya')
  end

  def market_products
    products.where(type: 'Product::Market')
  end

end
