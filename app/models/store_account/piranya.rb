class StoreAccount::Piranya < StoreAccount

  def import
    client.get_categories.each do |remote_category|
      puts remote_category[:name]
      category = Category.where(name: remote_category[:name]).first_or_create!
      short_products_info = client.get_products_from_category(remote_category)
      products_urls = short_products_info.map { |s| s[:url] }
      exist_products = Product.includes(:source_product).where(category: category, source_products: { url: products_urls })
      exist_product_ids = exist_products.map(&:id)
      # disable products if not exist
      #Product.where(category: category).where.not(id: exist_product_ids)
      SourceProduct::Piranya.includes(:product)
        .where(products: { category_id: category.id })
        .where.not(products: { id: exist_product_ids })
        .each { |sp| sp.product.reject! }
      #Compare prices
      exist_products.each do |exist_product|
        short_product_info = short_products_info.detect { |s| s[:url] == exist_product.source_product.url }
        remote_price = short_product_info[:price]
        remote_retail_price = short_product_info[:retail_price]
        unless exist_product.source_product.price.to_f == remote_price.to_f
          exist_product.source_product.price = remote_price
          exist_product.source_product.parsed_json[:retail_price] = remote_retail_price
          exist_product.save!
          exist_product.update_from_source
          exist_product.to_moderate!
        end
        if exist_product.aasm_state != 'published'
          exist_product.to_moderate!
        end
      end
      exist_urls = exist_products.map { |p| p.source_product.url }
      #select not exists products
      short_products_info.select { |s| exist_urls.exclude?(s[:url]) }.each do |short_product_info|
        #it can be because of special offer category and e.t.c.
        next if SourceProduct::Piranya.select(:id, :url).where(url: short_product_info[:url]).any?
        remote_product = client.get_product(short_product_info[:url])
        if remote_product && remote_product[:images].any? && remote_product[:description].present?
          source_product = SourceProduct::Piranya.create_from_source(remote_product, self)
          product = source_product.create_product!
          product.to_moderate! if product #product can be not present if source_product is not valid
        end
      end
    end
  end

  def client
    @client ||= ::Client::Piranya.new(self)
  end

end
