class Role < ApplicationRecord

  validates :name, presence: true, uniqueness: true

  has_and_belongs_to_many :users

  before_save :downcase_the_name

  protected

  def downcase_the_name
    self.name = self.name.downcase
  end

end
