class OrderMailer < ApplicationMailer
  default from: "noreply@fisher-secrets.ru"

  def complete_order_customer(order_id)
    @order = Order.find(order_id)
    mail(to: @order.email, subject: 'Совершен заказ. FISHER-SECRETS.RU')
  end

  def complete_order_vendor(order_id)
    @order = Order.find(order_id)
    mail(to: 'nikulinaleksandr@gmail.com', subject: 'Совершен заказ. FISHER-SECRETS.RU')
  end

end
