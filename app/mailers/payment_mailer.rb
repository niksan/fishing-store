class PaymentMailer < ApplicationMailer
  default from: "payments@fisher-secrets.ru"

  def complete_payment(payment_id)
    @payment = Payment.find(payment_id)
    mail(to: 'nikulinaleksandr@gmail.com', subject: 'Проведен платеж. FISHER-SECRETS.RU')
  end

end
