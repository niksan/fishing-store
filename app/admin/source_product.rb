ActiveAdmin.register SourceProduct do
  scope :all, default: true
  scope :valid
  scope :invalid

  filter :name

  index do
    id_column
    column :name
    column :price
    column :url
    column :is_valid
    column :created_at
    column :updated_at
    actions
  end

end
