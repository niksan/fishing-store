ActiveAdmin.register Payment do
  scope :all, default: true
  scope :initialized
  scope :rejected
  scope :confirmed

  index do
    id_column
    column :aasm_state
    column :amount
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :aasm_state
      row :amount
      row :created_at
      raw :updated_at
    end
    active_admin_comments
  end

end
