ActiveAdmin.register User do
  filter :email
  permit_params :spinning36

  index do
    id_column
    column :email
    column :spinning36
    column :created_at
    column :updated_at
    actions
  end

  form html: { multipart: true } do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :email, input_html: { readonly: true }
      f.input :spinning36
    end
    f.actions
  end

  member_action :become do
    return unless current_user.has_role?(:super_admin)
    sign_in(:user, resource, { bypass: true })
    redirect_to(root_url)
  end

  action_item :become, only: [:show, :edit] do
    link_to 'Become', become_admin_user_path(resource)
  end

end
