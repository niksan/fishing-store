ActiveAdmin.register ProductImage do
  permit_params :product_id, :main, :file

  index do
    id_column
    column :product_id
    column :main
    column :file do |pi|
      image_tag pi.file.thumb.to_s
    end
    actions
  end

  show do
    attributes_table do
      row :product
      row :main
      row :file do |pi|
        image_tag pi.file.thumb.to_s
      end
    end
  end

  form html: { multipart: true } do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :product, as: select2, collection: Product.published
      f.input :main
      f.input :file
    end
    f.actions
  end

end
