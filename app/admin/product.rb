ActiveAdmin.register Product do
  permit_params :category_id, :brand_id, :name, :price, :description, :pieces_per_pack, :min_order, :is_valid,
                :quantity, :main, :special_offer,
                product_images_attributes: [:id, :main, :file, :_destroy],
                field_values_attributes: [:id, :field_id, :value, :_destroy],
                reviews_attributes: [:id, :name, :value, :type, :product_id, :_destroy]

  scope :all, default: true
  scope :initialized
  scope :moderation
  scope :published
  scope :rejected
  scope :moderated_price
  scope :with_source_product
  scope :without_source_product

  filter :name
  filter :price
  filter :min_price
  filter :category
  filter :brand
  filter :special_offer
  filter :created_at
  filter :updated_at

  index do
    id_column
    column :name do |p|
      link_to p.name, product_path(p), target: '_blank'
    end
    column :price
    column :source_price do |p|
      p.source_product.try(:price)
    end
    column :category
    column :brand
    column :aasm_state
    column :source_url do |p|
      link = p.source_product.try(:url)
      link_to 'link', link, target: '_blank' if link.present? 
    end
    column :updated_at
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :name do |p|
        link_to p.name, product_path(p), target: '_blank'
      end
      row :price
      row :source_price do |p|
        p.source_product.try(:price)
      end
      row :min_price
      row :category
      row :brand
      row :min_order
      row :source_url do |p|
        link = p.source_product.try(:url)
        link_to link, link, target: '_blank' if link.present?
      end
      raw :is_valid
      raw :special_offer
    end
    active_admin_comments
  end

  form html: { multipart: true } do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :category, as: :select2
      f.input :brand, as: :select2
      f.input :name
      f.input :price
      f.input :description
      f.input :pieces_per_pack
      f.input :min_order
      f.input :is_valid
      f.input :quantity
      f.input :special_offer
    end
    f.has_many :product_images, heading: 'Images', allow_destroy: true do |pi_f|
      pi_f.input :main, as: :boolean
      pi_f.input :file, as: :file, :hint => image_tag(pi_f.object.file.thumb.url)
    end
    f.has_many :field_values, heading: 'Options', allow_destroy: true do |pi_f|
      pi_f.input :field, as: :select2
      pi_f.input :value
    end
    f.has_many :reviews, heading: 'Reviews', allow_destroy: true do |pi_f|
      pi_f.input :name
      pi_f.input :value
      pi_f.input :type, as: :select2, collection: ['Review::Youtube']
    end
    f.actions
  end

  member_action :publish do
    resource.publish!
    redirect_to admin_product_path(resource)
  end

  member_action :reject do
    resource.reject!
    redirect_to admin_product_path(resource)
  end

  member_action :moderate do
    resource.to_moderate!
    redirect_to admin_product_path(resource)
  end

  action_item :publish, only: [:show, :edit], if: -> { resource.moderation? } do
    link_to 'Publish', publish_admin_product_path(resource)
  end

  action_item :reject, only: [:show, :edit], if: -> { !resource.rejected? } do
    link_to 'Reject', reject_admin_product_path(resource)
  end

  action_item :moderate, only: [:show, :edit], if: -> { !resource.moderation? } do
    link_to 'To moderate', moderate_admin_product_path(resource)
  end

end
