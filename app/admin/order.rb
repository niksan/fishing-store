ActiveAdmin.register Order do
  filter :email

  scope :all, default: true
  scope :initialized
  scope :ready_to_pay
  scope :paid
  scope :delivering
  scope :completed
  scope :rejected

  index do
    id_column
    column :email
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :email
      row :first_name
      row :last_name
      row :phone_number
      row :aasm_state
      row :min_order
      raw :shipping_city do |s|
        s.destination_name
      end
      raw :shipping_address
      raw :shipping_price
      raw :order_id
    end
    active_admin_comments
  end

end
