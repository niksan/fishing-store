ActiveAdmin.register Page do
  filter :name
  permit_params :title, :body, :code, :type

  index do
    id_column
    column :code
    column :title
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :code
      row :title
      row :body
      row :created_at
      row :updated_at
    end
  end

  form html: { multipart: true } do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :code
      f.input :title
      f.input :body
    end
    f.actions
  end

end
