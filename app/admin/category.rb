ActiveAdmin.register Category do
  filter :name
  filter :product_prefix
  permit_params :name, :human_name, :description, :product_prefix, :published

  index do
    id_column
    column :name
    column :human_name
    column :product_prefix
    column :published
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :name
      row :human_name
      row :description
      row :product_prefix
      row :published
      row :created_at
      row :updated_at
    end
  end

  form html: { multipart: true } do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :name
      f.input :human_name
      f.input :description, as: :ckeditor
      f.input :product_prefix
      f.input :published
    end
    f.actions
  end

end
