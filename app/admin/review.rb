ActiveAdmin.register Review do
  filter :name
  permit_params :name, :value, :product_id, :type

  index do
    id_column
    column :name
    column :value
    column :product
    column :type
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :name
      row :value
      row :product
      row :type
      row :created_at
      row :updated_at
    end
  end

  form html: { multipart: true } do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :name
      f.input :value
      f.input :product, as: :select2, collection: Product.published
      f.input :type, as: :select2, collection: ['Review::Youtube']
    end
    f.actions
  end

end
