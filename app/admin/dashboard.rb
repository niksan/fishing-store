ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do

     columns do
       published_size = Product.published.count
       h2 "Published - #{published_size}"

       panel "source_price / price" do
         @max_price = Product.published.maximum(:price).to_i
         @prices_range = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 2000, 3000, 4000, 5000, 7500, 10000, 20000, @max_price]
         prev_val = 0
         @data1 = {}
         @data11 = {}
         @prices_range.each do |v|
           products_count = Product.with_source_product.published.where('products.price > ? AND products.price <= ?', prev_val, v).count
           source_products_count = Product.with_source_product.published.includes(:source_products)
             .where('source_products.price > ? AND source_products.price <= ?', prev_val, v).count
           @data1[v] = products_count.to_i
           @data11[v] = source_products_count.to_i
           prev_val = v
         end
         render partial: 'source_price_price', locals: { data1: @data1, data2: @data11 }
       end

       panel "products size / price_margin" do
         @margin_range = 10.step(120, 5).to_a
         @margin_range << 1000
         prev_val = 0
         @data2 = {}
         @margin_range.each do |v|
           batch_size = Product.published.where('price_margin > ? AND price_margin <= ?', prev_val, v).count
           @data2[v] = batch_size
           prev_val = v
         end
         render partial: 'produts_size_price_margin', locals: { data: @data2 }
       end

       panel "products size / retail_margin_price" do
         prev_val = 0
         @data4= {}
         @margin_range.each do |v|
           batch_size = Product.published.where('retail_price_margin > 0').where('retail_price_margin > ? AND retail_price_margin <= ?', prev_val, v).count
           @data4[v] = batch_size
           prev_val = v
         end
         render partial: 'produts_size_retail_price_margin', locals: { data: @data4 }
       end

       panel "price / retail_price_margin" do
         prev_val = 0
         @data3 = {}
         @prices_range.each do |v|
           avg_val = Product.published.where('price > ? AND price <= ?', prev_val, v).average(:retail_price_margin)
           @data3[v] = avg_val.to_i
           prev_val = v
         end
         render partial: 'price_price_margin', locals: { data: @data3 }
       end

       panel "category / price_margin" do
         prev_val = 0
         @data5 = {}
         pre_data5 = Product.published.group(:category_id).average(:price_margin)
         pre_cat_data = Category.where(id: pre_data5.keys).pluck(:id, :name)
         cat_data = {}
         pre_cat_data.each{|d| cat_data[d[0]] = d[1]}
         pre_data5.each{|k, v| @data5[cat_data[k]] = v.to_i}
         render partial: 'category_price_margin', locals: { data: @data5.sort_by{ |k, v| v }.to_h }
       end

       panel "category / retail_price_margin" do
         prev_val = 0
         @data6 = {}
         pre_data6 = Product.published.where('retail_price_margin > 0').group(:category_id).average(:retail_price_margin)
         pre_cat_data = Category.where(id: pre_data6.keys).pluck(:id, :name)
         cat_data = {}
         pre_cat_data.each{|d| cat_data[d[0]] = d[1]}
         pre_data6.each{|k, v| @data6[cat_data[k]] = v.to_i}
         render partial: 'category_retail_price_margin', locals: { data: @data6.sort_by{ |k, v| v }.to_h }
       end


     end

  end
end
