ActiveAdmin.register Brand do
  filter :name
  permit_params :name, :description, :image

  index do
    id_column
    column :name
    column :image do |r|
      image_tag r.image.thumb.to_s
    end
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :name
      row :image do |r|
        image_tag r.image.thumb.to_s
      end
      row :created_at
      row :updated_at
    end
  end

  form html: { multipart: true } do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :name
      f.input :description, as: :ckeditor
      f.input :image
    end
    f.actions
  end

end
