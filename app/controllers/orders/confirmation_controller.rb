class Orders::ConfirmationController < ApplicationController
  authorize_resource class: 'Order'

  def new
    @order = Order.find_by(public_id: params[:order_id], aasm_state: 'initialized')
    @shop_id = Rails.application.secrets[:yandex_kassa][:shop_id]
    @scid = Rails.application.secrets[:yandex_kassa][:scid]
    @customer_number = current_user.try(:id).to_i
    unless @order.can_make_order?
      flash[:error] = 'Ошибка заказа, проверьте корректность данных.'
      redirect_to edit_order_path(params[:order_id])
    end
  end

end
