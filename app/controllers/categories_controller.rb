class CategoriesController < ApplicationController
  load_and_authorize_resource
  include WebshopCommon

  before_action :get_sort_type, only: [:index, :show]

  SORT_TYPES = [
    { name: :price_asc, ar: :min_price, human_name: 'По возрастанию цены', default: true  },
    { name: :price_desc, ar: { min_price: :desc }, human_name: 'По уменьшению цены' },
    { name: :name_asc, ar: :name, human_name: 'По названию' }
  ]

  def index
    own_products = index_products_collection
    @products = products_collection(own_products).order(:category_id, :name, :price)
  end

  def show
    @category = Category.find(params[:id])
    @products = products_collection(category_products(@category))
    @brand_name = ''
    if params[:brand].present?
      @brand = Brand.find_by_friendly_id_slug(params[:brand])
      brand_name = " - #{@brand.name}"
    end
    @website_breadcrumbs << { name: @category.human_name, url: category_path(@category) }
    @website_breadcrumbs << { name: "<b>#{ @brand.name }</b>", url: category_path(@category, brand: @brand.slug) } if @brand
    @current_page_title = { name: @category.human_name, url: category_path(@category) }
    @website_title = "#{@category.name}#{brand_name}. #{@website_title}"
  end

  private

  def index_products_collection
    Product.published.special_offer
  end

  def category_products(category)
    category.piranya_products
  end

  def get_sort_type
    @sort_type = params[:sort_type].to_s.to_sym
    default_sort_name = SORT_TYPES.detect { |s| s[:default] }[:name]
    @sort_type = default_sort_name unless SORT_TYPES.map{ |s| s[:name] }.include?(@sort_type.try(:to_sym))
  end

  # sorted, filtered, paginated products
  def products_collection(source_collection=Product::Piranya.all)
    sort = SORT_TYPES.detect { |s| s[:name] == @sort_type }[:ar]
    search_result = Product::Search.build(params, source_collection)
    result = search_result.ready_to_show.order(sort)
    result = yield(result) if block_given?
    result.page(params[:page])
  end

end
