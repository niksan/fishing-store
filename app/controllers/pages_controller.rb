class PagesController < ApplicationController
  load_and_authorize_resource

  def show
    @current_page_title = { name: @page.title, url: page_path(@page)}
    @website_title = "#{@page.title}. " + @website_title
  end
  
end
