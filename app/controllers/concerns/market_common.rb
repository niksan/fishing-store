module MarketCommon
  extend ActiveSupport::Concern
  included do
    layout 'market'
    before_action :menu_categories
    before_action :update_breadcrumbs
  end

  def menu_categories
    @tree_hash = Category::Market.tree_hash
  end

  def update_breadcrumbs
    @website_breadcrumbs[0] = { name: 'Маркет', url: market_categories_path }
  end

  private

  def reload_vars
    @current_page_title = { name: 'Маркет', url: market_categories_path }
  end
  
end
