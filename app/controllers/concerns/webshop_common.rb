module WebshopCommon
  extend ActiveSupport::Concern
  included do
    layout 'shop'
    before_action :menu_categories
  end

  def menu_categories
    @tree_hash = Category.tree_hash
  end

end
