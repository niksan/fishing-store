class ProductsController < ApplicationController
  load_and_authorize_resource
  include WebshopCommon

  def show
    @product = Product.where(type: product_classes)
                      .includes(:category, :product_images, field_values: :field)
                      .find(params[:id])
    @category = @product.category
    @brand = @product.brand
    @website_breadcrumbs << { name: @product.category.human_name, url: category_path(@product.category) }
    @current_page_title = { name: @product.category.human_name, url: category_path(@product.category)}
    prefix = @product.category.product_prefix
    prefix += " " unless prefix.blank?
    @website_title = "#{prefix}#{@product.full_name}. В интернет-магазине Fisher-Secrets"
  end

  private

  def product_classes
    ['Product::Piranya', 'Product', nil]
  end

end
