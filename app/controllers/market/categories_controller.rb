class Market::CategoriesController < CategoriesController
  include MarketCommon

  private

  def index_products_collection
    Product::Market.published
  end

  def category_products(category)
    category.market_products
  end

end
