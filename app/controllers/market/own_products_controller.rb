class Market::OwnProductsController < ApplicationController
  include MarketCommon
  layout 'application'
  load_and_authorize_resource class: 'Product::Market'

  def index
    @products = Product::Market.where(user_id: current_user)
  end

  def publish
    @product.publish!
  end

  def to_modetarion
    @product.to_moderation!
  end

end
