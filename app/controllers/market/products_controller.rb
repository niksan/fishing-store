class Market::ProductsController < ProductsController
  #load_and_authorize_resource :product
  load_and_authorize_resource
  include MarketCommon

  def new
    @product = Product::Market.new
    @product.product_images.build(main: true)
  end

  def create
    @product = MarketProductServiceObject.create(
      product_params,
      current_user
    )
    if @product.persisted?
      @product.to_moderate!
      @product.publish!
      Category::Market.clear_cache
      redirect_to(market_categories_path)
    else
      @product.product_images.build(main: true)
      render :new
    end
  end

  private

  def product_params
    params.require(:product_market).permit(:name,  :category_id, :description, :price, product_images_attributes: [:file])
  end

  def product_classes
    ['Product::Market']
  end

end
