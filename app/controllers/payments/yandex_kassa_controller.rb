class Payments::YandexKassaController < PaymentsController
  
  def check
    begin
      order = Order.find(params['orderNumber'])  
      if valid_hash?(params, 'checkOrder')
        # Успешно. Магазин дал согласие и готов принять перевод.
        ActiveRecord::Base.transaction do
          @code = 0
          order.to_ready_to_pay!
          payment = Payment::YandexKassa.create!(order_id: order.id, amount: order.total_price, external_data: params.to_unsafe_hash)
        end
      else
        # Ошибка авторизации. Несовпадение значения параметра md5 с результатом расчета хэш-функции. Окончательная ошибка.
        @code = 1
      end
      # Отказ в приеме перевода. Отказ в приеме перевода с заданными параметрами. Окончательная ошибка.
      right_amount = (params['orderSumAmount'].to_f == order.total_price.to_f)
      @code = 100 unless (order.can_make_order? && right_amount)
    rescue => e
      # Ошибка разбора запроса. Магазин не в состоянии разобрать запрос. Окончательная ошибка.
      Rails.logger.error e
      @code = 200
    end
    @performed_date_time = Time.zone.now.strftime('%Y-%m-%dT%H:%M:%S.000%:z') 
    @invoice_id = params['invoiceId']
    @shop_id = params['shopId']
    @order_sum_amount = params['orderSumAmount']
    session[:browser_session_hash] = nil
    render '/payments/yandex_kassa/check.xml.builder', formats: [:xml]
  end

  def aviso
    begin 
      @performed_date_time = Time.zone.now.strftime('%Y-%m-%dT%H:%M:%S.000%:z')
      @invoice_id = params['invoiceId']
      @shop_id = params['shopId']
      order = Order.find(params['orderNumber'])  
      payment = order.payment 
      payment.update_attributes(external_data: params.to_unsafe_hash)
      payment.confirm!
      order.to_pay!
      @code = 0
    rescue => e
      # Ошибка разбора запроса. Магазин не в состоянии разобрать запрос. Окончательная ошибка.
      Rails.logger.error e
      @code = 200
    end
    # Ошибка авторизации. Несовпадение значения параметра md5 с результатом расчета хэш-функции. Окончательная ошибка.
    @code = 1 unless valid_hash?(params, 'paymentAviso')
    render '/payments/yandex_kassa/aviso.xml.builder', formats: [:xml]
  end

  def success
    redirect_to root_path
  end

  private

  def valid_hash?(params, action='checkOrder')
    shop_password = Rails.application.secrets[:yandex_kassa][:shop_password]
    str = "#{action};#{params['orderSumAmount']};#{params['orderSumCurrencyPaycash']};#{params['orderSumBankPaycash']};#{params['shopId']};#{params['invoiceId']};#{params['customerNumber']};#{shop_password}"
    md5 = Digest::MD5.hexdigest(str)
    params['md5'] == md5.upcase
  end

end
