class SitemapController < ApplicationController

  def index
    @products = Product.published.order(:category_id, :name)
    @categories = Category.tree_hash
    @pages = Page.all
    respond_to :xml
  end 

end
