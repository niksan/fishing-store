class OrderItemsController < ApplicationController
  load_and_authorize_resource

  def create
    @result = {}
    order = current_order
    unless order
      order = Order.create!(
        aasm_state: 'initialized',
        user_id: current_user.try(:id),
        browser_session_hash: session[:browser_session_hash],
        public_id: Digest::MD5.hexdigest(session[:browser_session_hash]),
        email: current_user.try(:email)
      )
    end
    order.add_product(params[:product_id])
    if order.errors.count.zero? && order.save
      @result[:status] = 'ok'
      session[:order_id] = order.id
      gflash success: 'Товар добавлен в корзину'
    else
      @result[:status] = 'error'
      error_message = order.errors.messages.map { |k, v| "#{ k }: #{ v.join('; ') }" }.join('/n')
      Rails.logger.error error_message
      @result[:error] = error_message
      gflash warning: 'Ошиба добавления в корзину'
    end
    respond_to do |format|
      format.js    
    end
  end

  def destroy
    order_item = OrderItem.find(params[:id])
    if can?(:destroy, order_item)
      order_item.destroy
    end
    redirect_back(fallback_location: root_path)
  end

end
