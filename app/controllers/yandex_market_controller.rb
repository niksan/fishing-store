class YandexMarketController < ApplicationController

  def index
    @shop_name = 'Fisher Secrets'
    @host = 'fisher-secrets.ru'
    @date = Time.zone.now.strftime("%Y-%m-%d %H:%M")
    @categories = Category.tree_hash
    @products_arr = Product.yandex_market_arr
    respond_to :xml
  end 

end
