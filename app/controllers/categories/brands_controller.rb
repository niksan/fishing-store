class Categories::BrandsController < CategoriesController
  layout 'shop'

  def index
    @category = Category.friendly.find(params[:category_id])
    brand_ids = @tree_hash[@category.id][:brands].map{ |b| b[:id]}
    @brands = Brand.where(id: brand_ids).order(:name)
    @current_page_title = { name: @category.human_name, url: category_path(@category) }
  end

end
