class ApplicationController < ActionController::Base
  protect_from_forgery prepend: true, with: :exception
  before_action :load_vars
  helper_method :current_order
  rescue_from ActionController::RoutingError, with: :error_404

  def access_denied(exception)
    redirect_to new_user_session_path, alert: exception.message
  end

  def current_order
    session[:browser_session_hash] ||= new_browser_session_hash
    order = Order.where(aasm_state: 'initialized')
                 .where('user_id = ? OR browser_session_hash = ?', current_user.try(:id), session[:browser_session_hash])
                 .last
    if order && order.user_id.nil? && current_user.try(:id).present?
      order.update_attributes(user_id: current_user.id)
    end
    order
  end

  protected

  def new_browser_session_hash
    Digest::SHA256.hexdigest("#{Time.now.utc.to_i}#{rand(1000)}")
  end

  private

  def load_vars
    @website_breadcrumbs = [{ name: 'Интернет-магазин', url: root_path }]
    @current_page_title = { name: 'Рыболовные товары', url: root_path }
    @footer_brands = Brand.where.not(image: nil)
    @website_title = 'Интернет-магазин рыболовных товаров Fisher-Secrets'
    @website_keywords = 'рыболовные товары Воронеж, рыбалка, спиннинг, катушка, удочка, фишер сикретс, фишер секретс'
    @website_description = 'Интернет-магазин рыболовных товаров. Доставка до двери, со склада в Воронеже.'
    reload_vars
  end

  def reload_vars
    #reinitialize vars. You can implement it at another classes
    nil
  end

  def error_404(exception = nil)
    if exception
      logger.info "Error 404: #{exception.message}"
    end                   
    redirect_to root_path, status: 404
  end

end
