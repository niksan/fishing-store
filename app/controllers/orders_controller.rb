class OrdersController < ApplicationController
  load_resource find_by: :public_id
  authorize_resource

  def edit
    @current_page_title = { name: 'Оформление заказа', url: edit_order_path(@order.id) }
    @website_breadcrumbs << { name: 'Оформление заказа', url: edit_order_path(@order.id) }
    @order.user_id = current_user.try(:id)
    @order.save!
  end

  def update
    @order.update_attributes!(order_params)
    redirect_to new_order_confirmation_path(@order.public_id)
  end

  private

  def order_params
    params.require(:order).permit(:first_name, :last_name, :email, :phone_number, :city, :region, :destination_id, :shipping_address)
  end

end
