class MarketProductServiceObject < BaseServiceObject

  PRODUCT_PARAMS = ['name', 'category_id', 'description', 'price']
  PRODUCT_IMAGES_PARAMS = ['product_images_attributes']

  def save
    product = Product::Market.new(
      product_params(@params).merge(is_valid: true, user_id: @user.id)
    )
    image_params = product_images_params(@params)['product_images_attributes']
      .try(:[], '0')
    if image_params
      ActiveRecord::Base.transaction do
        image_params['main'] = true #first image must be main
        product.save
        product.product_images.create(image_params)
      end
    else
      product.valid? #add product validation errors
      product.errors.add(:product_images, "Отсутствует изображение") 
    end
    product
  end

  private

  def product_params(params)
    params.select { |param| param.in?(PRODUCT_PARAMS) } 
  end

  def product_images_params(params)
    params.select { |param| param.in?(PRODUCT_IMAGES_PARAMS) }
  end

end
