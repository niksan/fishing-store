class BaseServiceObject
  
  def initialize(strong_params, user)
    @params = strong_params
    @user = user
  end

  def save
    raise "You must implement #save method"
  end

  class << self
    
    def create(strong_params, user)
      new(strong_params, user).save
    end

  end

end
