class BrandImageUploader < CarrierWave::Uploader::Base

  include UploadImage

  version :thumb do
    process resize_and_pad: [100, 50, '#f0f0f0', 'Center']
  end

  version :small do
    process resize_and_pad: [420, 453, "#ffffff", "Center"]
  end

  version :medium do
    process resize_to_fill: [640, 480]
  end

end
