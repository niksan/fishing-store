class ProductImageUploader < CarrierWave::Uploader::Base

  include UploadImage

  version :thumb do
    process resize_and_pad: [107, 107, "#ffffff", "Center"]
  end

  version :small do
    process resize_and_pad: [420, 453, "#ffffff", "Center"]
  end

  version :medium do
    process resize_and_pad: [460, 570, "#ffffff", "Center"]
  end

end
