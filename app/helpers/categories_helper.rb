module CategoriesHelper

  def sorted_page_url(sort_type)
    url = request.original_url.split('?')
    url_params = Rack::Utils.parse_nested_query(url[1])
    url_params['sort_type'] = sort_type
    "#{ url[0] }?#{ url_params.to_param }"
  end

end
