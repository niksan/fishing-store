cache('yandex_market-xml', expires_in: 1.day) do

  xml.instruct!
  xml.declare! :DOCTYPE, :yml_catalog, :SYSTEM, 'shops.dtd'

  xml.yml_catalog date: @date do
    xml.shop do

      xml.name @shop_name
      xml.company @shop_name
      xml.url root_url(host: @host, protocol: 'https', port: nil)

      xml.currencies do
        xml.currency id: 'RUR', rate: 1, plus: 0
      end


      xml.categories do
        @categories.each do |cat_id, cat_opt|
          cat_params = { id: cat_id }
          cat_params[:parent_id] = cat_opt[:parent_id] if cat_opt[:parent_id] > 0
          xml.category(cat_opt[:human_name], cat_params)
        end
      end

      xml.tag!('delivery-options') do
        xml.option cost: 1000, days: 2
      end

      xml.offers do
        @products_arr.each do |product|
          xml.offer(id: product[:id], type: 'vendor.model',  available: true) do
            xml.vendor product[:brand_name]
            xml.model product[:name]
            xml.url product[:url]
            xml.price product[:price]
            xml.currencyId 'RUR'
            xml.categoryId product[:category_id]
            xml.picture product[:picture_url]
            xml.delivery 'true'
            xml.description product[:description] unless product[:description].blank?
            xml.cpa 1 # участие предложений в программе «Заказ на Маркете».
            xml.tag!('min-quantity', product[:min_order]) if product[:min_order] > 1
          end
        end
      end

    end
  end

end
