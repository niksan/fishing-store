cache('sitemap-xml', expires_in: 5.minutes) do

  xml.instruct!
  xml.urlset "xmlns" => "http://www.sitemaps.org/schemas/sitemap/0.9" do

    xml.url do
      xml.loc "https://fisher-secrets.ru"
      xml.priority 1.0
    end

    @pages.each do |page|
      xml.url do
        xml.loc page_url(page)
        xml.priority 0.8
      end
    end

    @categories.each do |id, category|
      xml.url do
        xml.loc category_brands_url(category[:slug])
        xml.priority 0.9
      end
    end

    @products.each do |product|
      xml.url do
        xml.loc product_url(product)
        xml.priority 0.9
      end
    end

  end

end
