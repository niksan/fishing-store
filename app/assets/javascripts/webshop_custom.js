jQuery(document).ready(function(){
  $('#guiest_id1').change(function() {
    url = $('#guiest_id1 option:selected').data('url');
    window.location = url;
  });

  $('a.add_to_cart').click(function(e){
    product_id = $(this).data('product-id');
    url = '/order_items.js'
    request_body = 'product_id=' + product_id
    $.ajax({
      method: 'POST',
      url: url,
      data: { product_id: product_id, quantity: 1 }
    }).done(function() {
      $( this ).addClass( "done" );
    });
    e.preventDefault();
  })

  $( function() {
    $( "#order_destination_name" ).autocomplete({
      source: "/delivery/sdek/autocomplete",
      minLength: 2,
      select: function (event, ui) {
        $("#order_destination_name").val(ui.item.label);
        $("#order_destination_id").val(ui.item.id);
        return false;
      }
    });
  } );

});
