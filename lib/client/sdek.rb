class Client::Sdek

  RU_CITIES_PATH = 'public/sdek_city_rus.xls'
  URL = {
    calculator: 'http://api.cdek.ru/calculator/calculate_price_by_json.php',
  }
  API_VERSION = '1.0'
  DEFAULT_TARIF_ID = 137 # склад-дверь
  DEFAULT_PRICE = 300.to_f

  attr_reader :cities_data

  def initialize
    @cities_data = get_cities_data
  end

  def calculate(receiver_city_id, sender_city_id, goods=nil)
    begin
      url = URL[:calculator]
      body = {
        'version' => '1.0',
        'dateExecute' => Date.tomorrow.strftime('%Y-%m-%d'),
        'senderCityId' => sender_city_id.to_s,
        'receiverCityId' => receiver_city_id.to_s,
        'tariffId' => '11',
        'goods' => [
          {
            'weight' => '0.5',
            'length' => '20',
            'width' => '20',
            'height' => '30'
          }
        ]
      }.to_json
      res = HTTParty.post(url, body: body)
      parse_resp_body(res.body)['result']['price'].to_f
    rescue => e
      Rails.logger.error(e)
      DEFAULT_PRICE   
    end
  end

  def city_data_by_name(name)
    @cities_data.select do |row|
      row[:city].match(/^#{ name }/i)
    end
  end

  private

  def parse_resp_body(data)
    data_to_parse = data.encode('utf-8') 
    JSON.parse(data_to_parse)
  end

  def default_headers
    { 'Content-Type' => 'application/json' }
  end

  def get_cities_data
    Rails.cache.fetch('sdek_cities_data') do
      res = []
      filename = Rails.root.join(RU_CITIES_PATH).to_s
      xls = Roo::Excel.new(filename)
      xls.each do |row|
        res << { id: row[0].to_i, city: row[2], region: row[3] }
      end
      res.shift
      res
    end
  end

end
