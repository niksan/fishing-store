class Client::Piranya < Client

  ENDPOINT = 'http://www.piranya-ltd.ru'
  
  PAGES = {
    login: '/?login=yes',
    webshop: '/market/'
  }

  def initialize(account)
    @account = account
  end

  def do!
    all_options = []
    get_categories.each do |category|
      puts category
      if category[:name] == 'Спиннинги'
        brands = []
        get_products_from_category(category).each do |short_product_info|
          product_info = get_product(short_product_info[:url])
          brands << product_info[:options].detect { |k| k[:name] == 'Брэнд' }.try(:[], :value)
        end
        puts brands.uniq
      end
    end
    nil
  end
  
  def product_is_present?(source_product)
    res = false
    login do |cookies|
      resp = get_page_content(source_product.url, cookies)
      doc = resp[:nokogiri]
      h2 = doc.css('#main .cont-hold h2').text
      res = (source_product.name == h2)
    end
    res
  end

  def get_product(url)
    puts url
    res = false
    Rails.cache.fetch("piranya_product_#{url}", expires_in: 1.hour) do
      login do |cookies|
        is_valid = true
        retail_price = nil
        page_content = get_page_content(url, cookies)
        doc = page_content[:nokogiri]
        return false if doc.css('#main').inner_html.match(/Элемент не найден./)
        name = doc.css('#main .cont-hold h2').text
        unformatted_product_price = if doc.css('#main .cont-hold .cart-box .row .col.gall form.buy-form strong.price s').empty?
          doc.css('#main .cont-hold .cart-box .row .col.gall form.buy-form strong.price').text.strip
        else
          doc.css('#main .cont-hold .cart-box .row .col.gall form.buy-form strong.price div')[0].text.strip
        end
        pre_retail_price = doc.css('#main .cont-hold .cart-box .row .col.gall form.buy-form .row ul li').detect { |li| li.text.match(/РРЦ/) }.try(:css,'strong').try(:text)
        retail_price = if pre_retail_price.to_f.zero?
          nil
        else
          to_price(pre_retail_price)
        end
        product_price = to_price(unformatted_product_price)
        unless unformatted_product_price == unformatted_product_price.match(/([\d[[:space:]],]+a)/).try(:[], 1)
          is_valid = false
          #raise "wrong product price format!!! - #{ url }, '#{ unformatted_product_price }'"
        end
        options = doc.css('#main .cont-hold .cart-box .row .col.teh ul.teh-list li').map do |option|
          { name: option.css('span').text.gsub(':', '').gsub(/^[[:space:]]|[[:space:]]$/, ''), value: option.css('strong').text.strip }
        end
        category_name = options.detect { |o| o[:name]=='Классификатор' }[:value]
        package = (options.detect { |o| o[:name]=='Упаковка' }[:value].match(/(\d+)/).try(:[], 1).try(:to_i) || 1)
        is_present = options.pop[:name]
        description = doc.css('#main .cont-hold .cart-box').inner_html.chomp.strip.match(/.*<h3>описание<\/h3>([.\s\S]*)/)[1].chomp.strip
        min_quantity = doc.css('#main .cont-hold .cart-box .col.gall form.buy-form .row ul li').detect { |li| li.text.strip.match(/мин\. партия:.*/) }.text.match(/мин\. партия: ?(\d+).*/).try(:[], 1).to_i
        is_valid = false if min_quantity.zero?
        pack = doc.css('#main .cont-hold .cart-box .col.gall form.buy-form .row ul li').detect { |li| li.text.strip.match(/фасовка:.*/) }.text.strip.match(/(\d+)/).try(:[], 1).to_i
        images = doc.css('#main .cont-hold .cart-box .col.gall ul.slider li.tool .tooltip.element img').map { |img| route_to(nil, img['src']) }
        pieces_per_pack = (package > pack) ? package : pack
        res = { name: name, price: product_price, retail_price: retail_price, options: options, is_present: is_present, description: description, min_order: min_quantity, pieces_per_pack: pieces_per_pack, images: images, is_valid: is_valid, source: prepare_html(page_content[:source]), category_name: category_name, url: url }
      end
      res
    end
  end

  def get_products_from_category(category)
    products = []
    puts 'Getting products'
    login do |cookies|
      1.upto(category[:pages]) do |page_number|
        products << Rails.cache.fetch("piranya_category_products_#{ category[:path] }_#{ page_number }", expires_in: 10.hours) do
          url = route_to(nil, "#{ category[:path] }&PAGEN_1=#{ page_number }")
          doc = get_page_content(url, cookies)[:nokogiri]
          doc.css('#add2basket .row.products .lpro-box .holder.clearfix').map do |product_block|
            is_valid = true
            product_name = product_block.css('.text-box strong.title a').text
            product_path = product_block.css('.text-box strong.title a')[0]['href'].to_s
            unformatted_product_price = product_block.css('.col.sum strong.price').text
            product_price = to_price(unformatted_product_price)
            unless unformatted_product_price == unformatted_product_price.match(/([\d[[:space:]],]+[[:space:]]a)/).try(:[], 1)
              #raise "wrong product price format!!! - #{ url }, '#{ unformatted_product_price }'"
              is_valid = false
            end
            { name: product_name, url: route_to(nil, product_path), price: product_price, is_valid: is_valid, category_name: category[:name], category_url: route_to(nil, category[:path]) }
          end
        end
      end
    end
    res = products.flatten
    res
  end

  def get_categories
    Rails.cache.fetch('piranya_categories', expires_in: 12.hour) do
      puts 'Getting categories'
      login do |cookies|
        url = route_to(:webshop)
        doc = get_page_content(url, cookies)[:nokogiri]
        categories = doc.css('#sidebar-sections ul.goods-nav li').map do |li|
          print '.' #show progress
          link = li.children.detect { |c| c.name == 'a' }
          products_path = "#{ link['href'] }?type=0"
          products_url = route_to(nil, products_path)
          products_doc = get_page_content(products_url, cookies)[:nokogiri]
          paginator_links = products_doc.css('#add2basket ul.pager li a')
          pages_size = if paginator_links.empty?
            1
          else
            last_link = paginator_links[paginator_links.size-2]['href'].to_s
            regexp_cond = "#{ to_regexp(products_path) }&PAGEN_1\=(\\d*)"
            last_link.match(/#{ regexp_cond }/)[1].to_i
          end
          { name: link.text.strip, path: products_path, pages: pages_size }
        end
        puts '|'
        categories
      end
    end
  end

  protected

  def get_page_content(url, cookies)
    headers = default_headers({ cookies: cookies })
    resp = HTTParty.get(url, headers: headers)
    nokogiri = Nokogiri::HTML(prepare_html(resp.body))
    { code: resp.code, source: resp.body, nokogiri: nokogiri }
  end

  def login
    params = Rails.cache.fetch("#{self.class.name}_login", expires_in: 1.hour) { do_login }
    yield(params) if block_given?
  end

  def do_login
    url = route_to(:login)
    body = {
      'backurl' => '%2F',
      'AUTH_FORM' => 'Y',
      'TYPE' => 'AUTH',
      'USER_REMEMBER' => 'Y',
      'Login' => '%C2%EE%E9%F2%E8',
      'USER_LOGIN' => @account.login,
      'USER_PASSWORD' => @account.password
    }
    resp = HTTParty.post(url, body: body)
    resp.headers['set-cookie']
  end

  private

  def route_to(page=nil, postfix=nil)
    "#{ ENDPOINT }#{ PAGES[page] }#{ postfix }"
  end

  def to_price(price)
    price.to_s.gsub(/[[:space:]]/, '').gsub(',', '.').to_f
  end

  def to_regexp(str)
    str.to_s.gsub('/', '\/').gsub('?', '\?')
  end

  def default_headers(params={})
    {
      'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
      #'Accept-Encoding' => 'gzip, deflate',
      'Accept-Language' => 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
      'Cache-Control' => 'max-age=0',
      'Connection' => 'keep-alive',
      'Cookie' => params[:cookies],
      'Host' => 'www.piranya-ltd.ru',
      'Upgrade-Insecure-Requests' => '1',
      'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
    }
  end

  def prepare_html(html)
    html.encode('UTF-8', 'windows-1251')
        .gsub("\"", '\'').gsub("\r", '').gsub("\t", '  ')
  end

end
