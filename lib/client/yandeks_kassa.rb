class Client::YandexKassa < Client
  
  ENDPOINT = 'https://money.yandex.ru'
  
  PAGES = {
    eshop: '/eshop.xml'
  }

  def check_order
    parameters = {
      'action' => 'checkOrder',
      'orderSumAmount' => '',
      'orderSumCurrencyPaycash' => 643,
      'orderSumBankPaycash' => 1001,
      'shopId' => Rails.application.secrets[:yandex_kassa][:shop_id],
      'invoiceId' => 0,
      'customerNumber' => 123123,
      'shopPassword' => 'test_passwd'
    }
  end

  private

  def md5_encode(body)
    Digest::MD5.hexdigest(body.to_s)
  end

end
