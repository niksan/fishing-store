class Client

  ENDPOINT = ''
  PAGES = { nil: nil }

  def login
    params = Rails.cache.fetch("#{self.class.name}_login", expires_in: 1.hour) { do_login }
    yield(params) if block_given?
  end

  private

  def route_to(page=nil, postfix=nil)
    "#{ ENDPOINT }#{ PAGES[page] }#{ postfix }"
  end

end
