namespace :orders do

  task clean_old_inactive_orders: :environment do
    Order.initialized.where('created_at < ?', 2.weeks.ago)
  end

end
