namespace :reprocess_images do

  task brands: :environment do
    Brand.find_each do |brand|
      begin
        brand.image.cache_stored_file! 
        brand.image.retrieve_from_cache!(brand.image.cache_name) 
        brand.image.recreate_versions!
        brand.save! 
      rescue => e
        puts  "ERROR: Brand: #{brand.id} -> #{e.to_s}"
      end
    end
    CarrierWave.clean_cached_files!
  end

end
