namespace :products do

  task autopublish: :environment do
    Product::Piranya.autocorrect!
    Product::Piranya.autopublish!
  end

  task clear: :environment do
    Product.joins("LEFT JOIN product_images ON product_images.product_id = products.id").where(product_images: { id: nil }).find_each do |p|
      p.destroy
    end
  end

  task update_prices: :environment do
    Product::Piranya.with_source_product.published.where(retail_price: nil).find_each do |p|
      new_price = p.source_product.price + (p.source_product.price * Product::MIN_COMMISSION_COEFF)
      p.update_attributes(price: new_price.to_f)
    end
  end

end
