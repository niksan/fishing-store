require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module FishingStore
  class Application < Rails::Application

    config.enable_dependency_loading = true
    config.autoload_paths << Rails.root.join('lib')
    config.autoload_paths << Rails.root.join('lib/clients')
    config.time_zone = 'Moscow'
    config.i18n.enforce_available_locales = false
    config.i18n.default_locale = :ru
    config.active_job.queue_adapter = :sidekiq
    config.assets.precompile += %w( ckeditor/* )

  end
end
