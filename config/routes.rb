Rails.application.routes.draw do
  authenticate :user, lambda { |u| u.has_role? :super_admin } do
    require 'sidekiq/web'
    Sidekiq::Web.set :session_secret, Rails.application.secrets[:secret_token]
    Sidekiq::Web.set :sessions, Rails.application.config.session_options
    mount Sidekiq::Web => '/sidekiq'
  end

  ActiveAdmin.routes(self)
  devise_for :users
  root to: 'categories#index'

  resources :categories, only: [:index, :show] do
    resources :brands, only: :index, controller: 'categories/brands'
  end
  resources :products, only: :show
  resources :pages, only: :show
  resources :cart, only: :index
  resources :sitemap, only: :index
  resources :yandex_market, only: :index
  resources :order_items, only: [:create, :update, :destroy]
  resources :orders, only: [:edit, :update] do
    resources :confirmation, controller: 'orders/confirmation'
  end
  resources :contacts, only: :index

  namespace :delivery do
    resources :sdek do
      get :autocomplete, on: :collection
    end
  end

  namespace :payments do
    resources :yandex_kassa do
      post :check, on: :collection
      post :aviso, on: :collection
      get :success, on: :collection
    end
  end

  namespace :market do
    resources :products
    resources :own_products
    resources :categories
  end

end
