set :application, 'fisher-secrets'
set :repo_url, 'git@gitlab.com:niksan/fishing-store.git'

set :deploy_to, '/srv/fisher-secrets'
set :bundle_without, [:development, :test]
set :scm, :git
set :format, :pretty
set :log_level, :debug
set :pty, true
set :rails_env, 'production'
set :migration_role, :app
set :migration_servers, -> { primary(fetch(:migration_role)) }
set :conditionally_migrate, true
set :assets_roles, [:web, :app]
set :assets_prefix, 'fisher-secrets'
set :rails_assets_groups, :assets
set :keep_assets, 2
set :keep_releases, 5
set :rvm_ruby_version, '2.5.0'

set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/uploads', 'public/assets')
set :linked_files, fetch(:linked_files, []).push('config/secrets.yml')

set :sidekiq_config, -> { File.join('/srv/fisher-secrets/current', 'config', 'sidekiq.yml') }

set :unicorn_pid, '/srv/fisher-secrets/current/tmp/pids/unicorn.pid'
set :unicorn_config, '/srv/fisher-secrets/current/config/unicorn/production.rb'

set :whenever_identifier, ->{ fetch(:application) }

before 'deploy:updated', 'deploy:fix_absent_manifest_bug' 
after 'deploy:publishing', 'deploy:restart'
after "deploy:finished", "deploy:setup_config"

task :add_default_hooks do
  after 'deploy:starting', 'sidekiq:quiet'
  after 'deploy:updated', 'sidekiq:stop'
  after 'deploy:reverted', 'sidekiq:stop'
  after 'deploy:published', 'sidekiq:start'
end

namespace :unicorn do
  desc 'Stop Unicorn'
  task :stop do
    on roles(:app) do
      if test("[ -f #{fetch(:unicorn_pid)} ]")
        execute :kill, capture(:cat, fetch(:unicorn_pid))
      end
    end
  end

  desc 'Start Unicorn'
  task :start do
    on roles(:app) do
      within current_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, "exec unicorn -c #{fetch(:unicorn_config)} -D"
        end
      end
    end
  end

  desc 'Reload Unicorn without killing master process'
  task :reload do
    on roles(:app) do
      if test("[ -f #{fetch(:unicorn_pid)} ]")
        execute :kill, '-s USR2', capture(:cat, fetch(:unicorn_pid))
      else
        error 'Unicorn process not running'
      end
    end
  end

  desc 'Restart Unicorn'
  task :restart
  before :restart, :stop
  before :restart, :start
end

namespace :deploy do
  task :restart do
    invoke 'unicorn:restart'
  end

  task :stop do
    invoke 'unicorn:stop'
  end

  task :setup_config do
    on roles(:app) do
      execute :sudo, "ln -nfs /srv/fisher-secrets/current/config/nginx.conf /etc/nginx/sites-enabled/fisher-secrets.ru"
      execute :sudo, 'service nginx restart'
    end
  end

  task :fix_absent_manifest_bug do
    on roles(:web) do
      within release_path do  execute :touch,
        release_path.join('public', fetch(:assets_prefix), 'manifest.tmp')
      end
    end
  end
end
