# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

every 6.hours do
  rake "piranya:sync"
end

every 1.day do
  rake "orders:clean_old_inactive_orders"
end

every 12.hours do
  rake "products:autopublish"
end

every 3.days do
  rake "products:clear"
end
