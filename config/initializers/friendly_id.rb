module FriendlyId
  module FinderMethods
  
    def find_by_friendly_id_slug(slug)
      FriendlyId::Slug.where(sluggable_type: friendly_id_config.model_class.to_s, slug: slug.to_s).last.try(:sluggable)
    end

  end
end

FriendlyId.defaults do |config|
  config.use :reserved

  config.reserved_words = %w(new edit index session login logout users admin
    stylesheets assets javascripts images)

end
