upstream unicorn {
  server unix:/tmp/unicorn.fisher-secrets.sock fail_timeout=0;
}


server {
  listen 80;
  server_name fisher-secrets.ru www.fisher-secrets.ru;
  return 301 https://$server_name$request_uri;
}

server {
  listen 443;
  ssl on;
  ssl_certificate /etc/ssl/fisher-secrets.ru.crt;
  ssl_certificate_key /etc/ssl/fisher-secrets.ru.key;
  server_name fisher-secrets.ru www.fisher-secrets.ru;
  root /srv/fisher-secrets/current/public;

  location ~/(assets|uploads)/(.*).(js|css|png|jpg|JPG|jpeg|gif|ico|woff|woff2|ttf)$ {
    expires 1y;
    log_not_found off;
  }

  location ~ ^/(assets|uploads)/ {
    root /srv/fishing-store/shared/public/;
    gzip_static on;
    expires     max;
    add_header  Cache-Control public;
    add_header  Last-Modified "";
    add_header  ETag "";
    break;
  }

  try_files $uri/index.html $uri @unicorn;
  location @unicorn {
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_redirect off;
    proxy_pass http://unicorn;
  }

  error_page 500 502 503 504 /500.html;
  client_max_body_size 20M;
  keepalive_timeout 10;
}

server {
  listen 443;
  server_name www.fisher-secrets.ru;
  return 301 https://fisher-secrets.ru$request_uri;
}
